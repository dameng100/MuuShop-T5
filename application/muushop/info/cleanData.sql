DROP TABLE IF EXISTS `muucmf_muushop_cart`;
DROP TABLE IF EXISTS `muucmf_muushop_coupon`;
DROP TABLE IF EXISTS `muucmf_muushop_delivery`;
DROP TABLE IF EXISTS `muucmf_muushop_messages`;
DROP TABLE IF EXISTS `muucmf_muushop_nav`;
DROP TABLE IF EXISTS `muucmf_muushop_order`;
DROP TABLE IF EXISTS `muucmf_muushop_product`;
DROP TABLE IF EXISTS `muucmf_muushop_product_cats`;
DROP TABLE IF EXISTS `muucmf_muushop_product_comment`;
DROP TABLE IF EXISTS `muucmf_muushop_product_extra_info`;
DROP TABLE IF EXISTS `muucmf_muushop_product_sell`;
DROP TABLE IF EXISTS `muucmf_muushop_service`;
DROP TABLE IF EXISTS `muucmf_muushop_user_address`;
DROP TABLE IF EXISTS `muucmf_muushop_user_coupon`;
